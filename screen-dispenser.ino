#include <genieArduino.h> 
#include <EEPROM.h>

Genie genie;

//Definicion de los pines

int MarcaArduino[] = {10,11};
int RESETLINE = 4;              // Pin para resetear la pantalla tactil

char keyvalue[9];
int temp = 0;
int counter = 0;
int index = 0;
int escala = 0;
int tiempo = 0;
int proporcion = 0;
int volumen = 0;

int tiempoSabor[] = {950,950,950,950,950,950,950,950,950,950};
bool activado[] = {0,0,0,0,0,0,0,0,0,0};
int BombaSabor[] = {35,37,39,41,43,45,32,34,47,49,51,53};

int tiempoMarca = 300;

int valor = 40;                  // Marca para lectura de la pantalla
int valorAntes = 40;

void setup() {

  Serial.begin(9600);  // Serial0 @ 9600 Baud
  genie.Begin(Serial);   // Use Serial0
  
  genie.AttachEventHandler(myGenieEventHandler);

   //Lectura de la memoria de configuracion
   
   for(int k = 0; k <= 9; k++){

      tiempoSabor[k] = (EEPROM.read(k*2)*100)+(EEPROM.read((k*2)+1));
     
   }
    
  pinMode(MarcaArduino[0], OUTPUT);
  pinMode(MarcaArduino[1], OUTPUT);

  for(int j = 0; j <= 11; j++){

      pinMode(BombaSabor[j], OUTPUT);

  }

  pinMode(RESETLINE, OUTPUT);  
  
  digitalWrite(MarcaArduino[0], HIGH);
  digitalWrite(MarcaArduino[1], HIGH);

     for(int j = 0; j <= 11; j++){

     if(j <= 1){
        digitalWrite(BombaSabor[j], LOW);
     }else{
       digitalWrite(BombaSabor[j], HIGH);
     }
  }

  
  
  
  // Reset the Display (change D4 to D2 if you have original 4D Arduino Adaptor)
  // THIS IS IMPORTANT AND CAN PREVENT OUT OF SYNC ISSUES, SLOW SPEED RESPONSE ETC
  // If NOT using a 4D Arduino Adaptor, digitalWrites must be reversed as Display Reset is Active Low, and
  // the 4D Arduino Adaptors invert this signal so must be Active High.
  // Set D4 on Arduino to Output (4D Arduino Adaptor V2 - Display Reset)
      digitalWrite(RESETLINE, 1);  // Reset the Display via D4
      delay(100);
      digitalWrite(RESETLINE, 0);  // unReset the Display via D4
      delay (3500); //let the display start up after the reset (This is important)

      genie.WriteContrast(10); // Valores de contraste 0-15
     
}

void loop() {

    //Los eventos se ejecutan desde la pantalla, este metodo direcciona a la funcion MyGenieEventHandler()
    genie.DoEvents();

}


//Funcion para activar bombas segun el boton seleccionado

int activarBombas(int sabor){

    if(sabor <=2 ){

        digitalWrite(BombaSabor[sabor - 1], HIGH);
        delay(tiempoMarca);
        digitalWrite(MarcaArduino[0], HIGH);
        digitalWrite(MarcaArduino[1], HIGH);
        delay(tiempoSabor[sabor - 1]);  
        digitalWrite(BombaSabor[sabor - 1], LOW);
        genie.WriteObject(GENIE_OBJ_USERBUTTON,valor,0);
        valor = 40;

    }else if(sabor == 11 || sabor == 12){

        digitalWrite(BombaSabor[sabor - 1], LOW);
        delay(tiempoMarca);
        digitalWrite(MarcaArduino[0], HIGH);
        digitalWrite(MarcaArduino[1], HIGH);
        delay(tiempoSabor[sabor - 3]);  
        digitalWrite(BombaSabor[sabor - 1], HIGH);
        genie.WriteObject(GENIE_OBJ_USERBUTTON,valor,0);
        valor = 40;

    }else{

        digitalWrite(BombaSabor[sabor - 1], LOW);
        delay(tiempoMarca);
        digitalWrite(MarcaArduino[0], HIGH);
        digitalWrite(MarcaArduino[1], HIGH);
        delay(tiempoSabor[sabor - 1]);  
        digitalWrite(BombaSabor[sabor - 1], HIGH);
        genie.WriteObject(GENIE_OBJ_USERBUTTON,valor,0);
        valor = 40;

    }
      
}

// Funcion principal ejecutada desde la pantalla

void myGenieEventHandler(void){

    int keyboardValue;
    genieFrame Event;
    genie.DequeueEvent(&Event);
    

    
    // Si se reporta un evento
    if (Event.reportObject.cmd == GENIE_REPORT_EVENT){

    // Si el evento que se reporta proviene de un boton de la pantalla

    if (Event.reportObject.object == GENIE_OBJ_KEYBOARD){

        temp = genie.GetEventData(&Event);
        if(temp >= 48 && temp <= 57 && counter <=3){
          keyvalue[counter] = temp;
          genie.WriteStr(0,keyvalue);
          counter = counter + 1;
          
        }  else if(temp == 8 && counter > 0){
              
              keyvalue[counter] = 0;
              genie.WriteStr(0,keyvalue);
              counter--;

          }
     }   
        
    }

    
    if (Event.reportObject.object == GENIE_OBJ_TRACKBAR){

      volumen = genie.GetEventData(&Event);

    }



    if (Event.reportObject.object == GENIE_OBJ_4DBUTTON){
      
      int valor1 = Event.reportObject.index;
      int activador = genie.GetEventData(&Event);
      
      if(valor1 <= 9){

         activado[valor1] = activador;

      }

      if(valor1 == 10){

         proporcion = 1;
         genie.WriteObject(GENIE_OBJ_4DBUTTON,11,0);

      }
      
      if(valor1 == 11){

         proporcion = 2;
         genie.WriteObject(GENIE_OBJ_4DBUTTON,10,0);
         
      }
      
    }

    if (Event.reportObject.object == GENIE_OBJ_SLIDER){
       
      escala = genie.GetEventData(&Event);
      tiempo = 200*(escala+1)*(volumen + 1);

    }

    
    if (Event.reportObject.object == GENIE_OBJ_USERBUTTON){


    // Guarda el indice del boton pulsado en valor
         valor = Event.reportObject.index;

    // Si el valor es igual a uno de los indices de los botones (0-5)
    

    if(valorAntes != valor){


          if( valor != 40 ){
          
          
          if( valor <= 5 || valor >= 8 && valor <= 11){

            genie.WriteObject(GENIE_OBJ_FORM,3,0);
            selector();
            activarBombas(valor + 1);
            genie.WriteObject(GENIE_OBJ_FORM,0,0);
            
          }

          if(valor == 6 || valor == 12){
            
            for(counter; counter >= 0; counter--){
              keyvalue[counter] = 0;
              genie.WriteStr(0,keyvalue);
              counter--;
            }  
              
            genie.WriteObject(GENIE_OBJ_FORM,2,0);
            genie.WriteObject(GENIE_OBJ_USERBUTTON,valor,0);
            valor = 40; 
            
          }

          if(valor == 13 || valor == 14 || valor == 17){

            genie.WriteObject(GENIE_OBJ_FORM,0,0);
            genie.WriteObject(GENIE_OBJ_USERBUTTON,valor,0);
            valor = 40; 

          }
          // Boton Confirmar configuracion
          if(valor == 15){

            genie.WriteObject(GENIE_OBJ_FORM,0,0);
            genie.WriteObject(GENIE_OBJ_USERBUTTON,valor,0);
            valor = 40;

            //Actualiza tiempo de bombas seleccionadas
            for(int a = 0; a <= 9; a++){
               if(activado[a] == 1){
                  tiempoSabor[a] = tiempo;
               }
            }
            

            //Escritura
            
           
            for(int k = 0; k <= 9; k++ ){

               EEPROM.update(k*2,tiempoSabor[k]/100);
               EEPROM.update((k*2)+1,tiempoSabor[k]%100);
               
            }
            
          }

          if(valor == 16){

            genie.WriteObject(GENIE_OBJ_FORM,0,0);
            genie.WriteObject(GENIE_OBJ_USERBUTTON,valor,0);
            valor = 40;

            //Actualiza tiempo de bombas seleccionadas
            for(int a = 0; a <= 9; a++){
               
                  tiempoSabor[a] = 950;
               
            }

            for(int k = 0; k <= 9; k++ ){

               EEPROM.update(k*2,tiempoSabor[k]/100);
               EEPROM.update((k*2)+1,tiempoSabor[k]%100);
               
            }

          }

          if(valor == 18 && keyvalue[0] == 51 && keyvalue[1] == 56 && keyvalue[2] == 55 && keyvalue[3] == 48){

            keyvalue[0] = {};
            keyvalue[1] = {};
            keyvalue[2] = {};
            keyvalue[3] = {};
            genie.WriteObject(GENIE_OBJ_FORM,4,0);
            genie.WriteObject(GENIE_OBJ_USERBUTTON,valor,0);
            valor = 40;
          }

          if(valor == 7){

            genie.WriteObject(GENIE_OBJ_FORM,1,0);
            genie.WriteObject(GENIE_OBJ_USERBUTTON,valor,0);
            valor = 40; 
            
          }                      
      
          }
          
         valorAntes = valor;  
         
    }
    
   }
   }
        
            


 
void selector(){

  switch(volumen){

    case 0: 

      digitalWrite(MarcaArduino[0], LOW);

    break;

    case 1: 

      digitalWrite(MarcaArduino[1], LOW);
    
    break;

    case 2: 

      digitalWrite(MarcaArduino[0], LOW);
      digitalWrite(MarcaArduino[1], LOW);
    
    break;


  }

}
